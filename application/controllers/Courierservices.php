<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 header('Access-Control-Allow-Origin: *');

 class Courierservices extends PIXOLO_Controller {
 	 function __construct()
 	 {
 	 	 parent::__construct();
          $this->load->model('Courierservices_model','model');

        
        //ABOUT THIS TABLE
       
        $this->tablename = "courierservices";
        

 	 }

   	 public function index()
   	 {
   	 	  $message['json']=$this->model->get_all();
   	 	  $this->load->view('json', $message);
   	 }

 }
