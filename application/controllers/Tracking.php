<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 header('Access-Control-Allow-Origin: *');

 class Tracking extends PIXOLO_Controller {
 	 function __construct()
 	 {
 	 	 parent::__construct();

        
        //ABOUT THIS TABLE
       $this->load->model('Tracking_model','model');
        $this->tablename = "tracking";
       

 	 }

   	 public function index()
   	 {
   	 	  $message['json']=$this->model->get_all();
   	 	  $this->load->view('json', $message);
   	 }
     public function gettrackingdata(){
         $invoiceid=$this->input->get('invoice_id');
         $message['json']=$this->model->gettrackingdata($invoiceid);
         $this->load->view('json',$message);
         
     }

 }